
FROM androidsdk/android-30

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git
# --- Node JS -------------------------------------
# Add the Node.js-maintained repositories to Ubuntu package source list
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
# The nodejs package contains the nodejs binary as well as npm
RUN apt-get install -y nodejs

# "build-essential" required, but were pre-installed in base image
RUN nodejs -v
RUN npm -v
RUN npm install -g ionic
RUN npm i @capacitor/core
RUN npm i -D @capacitor/cli
RUN npm i npx
RUN echo "done"
